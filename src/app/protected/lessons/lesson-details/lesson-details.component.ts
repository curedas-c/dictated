import { Component, OnInit } from '@angular/core';
import { PopupService } from 'src/app/@shared/services/popup.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HttpService, Lesson } from 'src/app/@core/services/http.service';

@Component({
  selector: 'app-lesson-details',
  templateUrl: './lesson-details.component.html',
  styleUrls: ['./lesson-details.component.scss']
})
export class LessonDetailsComponent implements OnInit {
  course_id: number;
  lessons: Lesson[];
  actual_lesson: Lesson;
  retryOption:boolean;
  isBackable:boolean = false;
  isNextable:boolean = true;

  constructor(
    private popup: PopupService,
    private http: HttpService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.course_id = parseInt(this.route.snapshot.paramMap.get('id'));
    console.log(this.course_id);
    this.getLessons();
  }

  getLessons() {
    this.http.getLessons(this.course_id).subscribe(
      (data) => { 
        this.lessons = data,
        this.actual_lesson = this.lessons[0];
      },
      (error) => { this.retryOption = true }
    )
  }

  setActualLesson(lesson: Lesson){
    this.actual_lesson = lesson;
    this.changeStatus();
  }

  goNext(){
    const nextLessonIndex = this.lessons.indexOf(this.actual_lesson) + 1;
    const nextLesson = this.lessons[nextLessonIndex];
    if(nextLesson){
      this.actual_lesson = nextLesson
    }
    this.changeStatus()
  }

  goBack(){
    const backLessonIndex = this.lessons.indexOf(this.actual_lesson) - 1;
    const backLesson = this.lessons[backLessonIndex];
    if(backLesson){
      this.actual_lesson = backLesson;
    }
    this.changeStatus()
  }

  changeStatus(){
    const checkNext = this.lessons.indexOf(this.actual_lesson) + 1;
    const checkBack = this.lessons.indexOf(this.actual_lesson) - 1;

    if(checkBack >= 0){ this.isBackable = true }
    else{
      this.isBackable = false
    }
    if(this.lessons[checkNext]){ this.isNextable = true }
    else{
      this.isNextable = false
    }
  }

  show_popup(color: string, message: string){
    this.popup.showPopupElement(color, message);
  }
}
