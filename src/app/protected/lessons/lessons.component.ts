import { Component, OnInit } from '@angular/core';
import { HttpService, Course } from 'src/app/@core/services/http.service';
import { LocalStorageService } from 'src/app/@shared/services/local-storage.service';
import { PopupService } from 'src/app/@shared/services/popup.service';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit {

  courses: Course[];
  retryOption: boolean = false;
  link = 'cours en ligne 1';

  constructor (
    private http: HttpService,
    private storage: LocalStorageService,
    private popup: PopupService
    ) { }

  ngOnInit(): void {
    this.storage.getItem('user').subscribe(
      (data) => { console.log(data) },
      (error) => {this.popup.showPopupElement('red', 'Veuillez vous reconnecter.', undefined, '/login')}
    )
    this.getCourses();
  }

  getCourses() {
    this.http.getCourses().subscribe(
      (data) => { this.courses = data },
      (error) => { this.retryOption = true }
    )
  }

}
