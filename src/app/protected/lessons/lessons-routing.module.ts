import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LessonsComponent } from './lessons.component';
import { LessonDetailsComponent } from './lesson-details/lesson-details.component';

const routes: Routes = [
  { path: ':id',  component:LessonDetailsComponent },
  { path: '', component: LessonsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LessonsRoutingModule { }
