import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LessonsRoutingModule } from './lessons-routing.module';
import { LessonsComponent } from './lessons.component';
import { LessonDetailsComponent } from './lesson-details/lesson-details.component';


@NgModule({
  declarations: [LessonsComponent, LessonDetailsComponent],
  imports: [
    CommonModule,
    LessonsRoutingModule
  ]
})
export class LessonsModule { }
