import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  
  //params for popup
  @Input() color: string;
  @Input() message: string;

  //event to emit when popup is about to closed
  @Output() popupClosed = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
