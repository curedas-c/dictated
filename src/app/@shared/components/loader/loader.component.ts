import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  @Input() color: string
  //event to emit when popup is about to closed
  @Output() loaderClosed = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }


}
