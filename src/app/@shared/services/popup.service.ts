import { Injectable } from '@angular/core';
import { NgElement, WithProperties } from '@angular/elements';
import { PopupComponent } from '../components/popup/popup.component';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  allPopups:any[] = [];

  constructor(private router: Router) { }

  // create custom-element to add the popup to the DOM.
  showPopupElement(color: string, message: string, time?:number ,redirectTo?: string) {
    // Create element
    const popupEl: NgElement & WithProperties<PopupComponent> = document.createElement('app-popup') as any;

    // Listen to the close event
    popupEl.addEventListener('popupClosed', () => {
      this.removeAllPopups();
      if ( redirectTo !== undefined ){
        this.router.navigate([redirectTo]);
      }
    });

    // Set the color
    popupEl.color = color;

    // Set the message
    popupEl.message = message;

    // Add to the DOM
    document.body.appendChild(popupEl);

    //remove from DOM
    setTimeout(() => {
      document.body.removeChild(popupEl);
    }, time ? time : 10000);
  }

  removeAllPopups(){
    const popups = document.body.querySelectorAll('app-popup');
    for (let index = 0; index < popups.length; index++) {
      document.body.removeChild(popups[index]);   
    }
  }
}
