import { Injectable } from '@angular/core';
import { NgElement, WithProperties } from '@angular/elements';
import { LoaderComponent } from 'src/app/@shared/components/loader/loader.component';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor() { }

   // create custom-element to add the popup to the DOM.
   showLoaderElement(color: string) {
    // Create element
    const popupEl: NgElement & WithProperties<LoaderComponent> = document.createElement('app-loader') as any;

    // Listen to the close event
    popupEl.addEventListener('loaderClosed', () => document.body.removeChild(popupEl) );

    // Set the loader color
    popupEl.color = color;

    // Add to the DOM
    document.body.appendChild(popupEl);
  }

}
