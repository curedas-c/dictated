import { Injectable } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private storage: LocalStorage) { }

  getItem(key: string): Observable<any> {
    return this.storage.getItem(key);
  }

  setItem(key: string, value:any): Observable<boolean> {
    return this.storage.setItem(key, value);
  }

  removeItem(key: string): Observable<boolean> {
    return this.storage.removeItem(key);
  }

  clearStorage(): Observable<boolean> {
    return this.storage.clear();
  }
}
