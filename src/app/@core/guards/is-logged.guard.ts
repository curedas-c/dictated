import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { LocalStorageService } from 'src/app/@shared/services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  constructor(private login: AuthService, private router: Router, private storage: LocalStorageService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let url: string = state.url;

      return this.checkLogin(url);
  }
  
  checkLogin(url: string): boolean {
    if (this.storage.getItem('user') != null || this.login.isLoggedIn) { return true; }

    // Store the attempted URL for redirecting
    this.login.redirectUrl = url;
    console.log(url);

    // Navigate to the login page with extras
    this.router.navigate(['/login']);
    return false;
  }
}
