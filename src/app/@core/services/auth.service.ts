import { Injectable } from '@angular/core';
import { LocalStorageService } from 'src/app/@shared/services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;

  constructor(private storage: LocalStorageService) { }

  // store the URL so we can redirect after logging in
  redirectUrl: string = '/lessons';

  login(user): void {
    this.isLoggedIn =true;
    this.storage.setItem('user', user);
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}
