import { Injectable } from '@angular/core';
/**
 * Global variables api
 */
import { environment } from '../../../environments/environment';
/**
 * http relatives
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse} from '@angular/common/http';
/**
 * rxjs objects
 */
import { throwError, Observable } from 'rxjs';
import { catchError, retry, shareReplay } from 'rxjs/operators';


export interface Course {
  id: number,
  title: string;
  img_url: string;
}

export interface Lesson {
  title: string;
  classement: string;
  video_url: string;
  body: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  userUrl: string = environment.userUrl;
  courseUrl: string = environment.courseUrl;
  lessonUrl: string = environment.lessonUrl;

  Headers = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  };
  courseList$: Observable<Course[]>
  lessonList$: Observable<Lesson[]>

  constructor(private http: HttpClient) {
    this.courseList$ = this.getCoursesObservable();
   }

  getCoursesObservable() {
    return this.http.get<Course[]>(this.courseUrl)
      .pipe(
        shareReplay(1),
        retry(1),
        catchError(this.handleError) // then handle the error
      );
  }
  getCourses() {
    return this.courseList$;
  }

  getLessons(courseId: number) {
    return this.http.get<Lesson[]>(this.lessonUrl + '/?course=' + courseId)
      .pipe(
        retry(1), // retry a failed request
        catchError(this.handleError) // then handle the error
      );
  }

  /**
   * Login requests
   */
  logUser(credentials:any) {
    return this.http.post(this.userUrl + '/login', credentials, this.Headers)
      .pipe(
        catchError(this.handleError)
      );
  }

  registerUser(credentials:any) {
    return this.http.post(this.userUrl + '/register', credentials, this.Headers)
      .pipe(
        catchError(this.handleError)
      );
  }

  resetUserPassword(credentials:any) {
    return this.http.post(this.userUrl + '/restart', credentials, this.Headers)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Http error!');
  };
}
