import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PopupComponent } from './@shared/components/popup/popup.component';

import { HttpService } from './@core/services/http.service';
import { PopupService } from './@shared/services/popup.service';
import { LoaderComponent } from './@shared/components/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    PopupComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpService, PopupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
