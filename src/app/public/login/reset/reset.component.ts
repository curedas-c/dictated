import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  @Output() formCredentials = new EventEmitter<object>();
  @Output() popupEvent = new EventEmitter<object>();
  defaultForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor() { }

  ngOnInit(): void {
  }

  raisePopup(color: string, message:string, time?:number, redirect?:string){
    /**
     * raise popup from parent component 
     * */
    if(redirect == undefined){
      this.popupEvent.emit({"color": color, "message": message});
    }
    else{
      this.popupEvent.emit({"color":color, "message": message, "time":time ,"redirect": redirect});
    }
  }
  
  submitForm(){
    /**
     * Emit data to parent component 
     * */
    this.formCredentials.emit(this.defaultForm.value);
  }

}
