import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  @Output() formCredentials = new EventEmitter<object>();
  @Output() popupEvent = new EventEmitter<object>();
  defaultForm = new FormGroup({
    email: new FormControl('test@example.com', [Validators.required,Validators.email]),
    password: new FormControl('example', [Validators.required]),
  });

  constructor() { }

  ngOnInit(): void {
  }

  raisePopup(color: string, message:string, time?:number, redirect?:string){
    /**
     * raise popup from parent component 
     * */
    if(redirect == undefined){
      this.popupEvent.emit({"color": color, "message": message});
    }
    else{
      this.popupEvent.emit({"color":color, "message": message, "time":time ,"redirect": redirect});
    }
  }
  submitForm(){
    console.log(this.defaultForm.value);
    /**
     * Emit data to parent component 
     * */
    this.formCredentials.emit(this.defaultForm.value);
  }
}
