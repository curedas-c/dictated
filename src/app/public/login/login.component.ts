import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpService } from 'src/app/@core/services/http.service';
import { AuthService } from 'src/app/@core/services/auth.service';
import { PopupService } from 'src/app/@shared/services/popup.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  work: string;
  credentials = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpService,
    private auth: AuthService,
    private popup: PopupService
    ) { 
      /** Add css class to body */
    document.body.className = "login-body";
    }

  ngOnInit(): void {
    /**
     * Get params from routerLink 
     * this.route.queryParams.subscribe(params => {
      console.log(params['work']);
    });
     * */
    this.work = this.route.snapshot.data['work'];
  }

  ngOnDestroy(): void {
    /** Remove eddaed css class from body */
    document.body.className = "";
  }

  logUser(credentials){
    this.http.logUser(credentials).subscribe(
      data => {
        if(data){
          /**
           * Log user if server accept 
           * */
          this.auth.login(data);
          /**
           * Redirect to saved url 
           * */
          this.router.navigate([this.auth.redirectUrl]);
        }
        else(
          console.log('False Credentials')
        )
      },
      (error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          this.popup.showPopupElement('alert-danger', 'Vérifiez votre connexion réseau et réessayez.')
        } else {
          this.popup.showPopupElement('alert-danger', 'Nom d\'utilisateur ou Mot de passe invalide')
        }
      }
    );
  }

  registerUser(credentials){
    this.http.registerUser(credentials).subscribe(
      data => {
        /**
           * show message and redirect to login 
           * */
          this.show_popup('alert-success', 'Votre compte est créé. Veuillez maintenant vous connecter', undefined, '/login')
      },
      (error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          this.popup.showPopupElement('alert-danger', 'Vérifiez votre connexion réseau et réessayez.')
        } else {
          this.popup.showPopupElement('alert-danger', 'Impossible de créer le compte')
        }
      }
    );
  }

  resetUserPassword(credentials){
    this.http.resetUserPassword(credentials).subscribe(
      data => {
        /**
           * show message and redirect to login 
           * */
          this.show_popup('alert-success', 'Un mail a été envoyé a cet adresse e-mail. Si toutefois elle est liée à un compte', undefined, '/login')
      },
      (error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          this.popup.showPopupElement('alert-danger', 'Vérifiez votre connexion réseau et réessayez.')
        } else {
          this.popup.showPopupElement('alert-danger', 'Une erreur s\est produite. Veuillez réssayer')
        }
      }
    );
  }

  show_popup(color: string, message: string, time?:number, link?:string){
    if ( link !== undefined ) { 
      this.popup.showPopupElement(color, message, time, link)
     }
    else { 
      this.popup.showPopupElement(color, message) 
    }
  }
}
