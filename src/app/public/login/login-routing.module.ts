import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, data: { work: 'login' } },
  { path: 'register', component: LoginComponent, data: { work: 'register' } },
  { path: 'forgot-password', component: LoginComponent, data: { work: 'reset' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
