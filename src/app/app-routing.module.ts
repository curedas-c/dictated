import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from './public/notfound/notfound.component';

import { IsLoggedGuard } from '../app/@core/guards/is-logged.guard';

const routes: Routes = [
  /**
   * Public routes 
   * */
  { 
    path: 'login',
    loadChildren: () => import('./public/login/login.module').then(m => m.LoginModule) 
  },
  { 
    path: 'page-introuvable', 
    component: NotfoundComponent 
  },
  /**
   * Protected routes
   * */
  { 
    path: 'account', 
    loadChildren: () => import('./protected/account/account.module').then(m => m.AccountModule),
    canActivate: [IsLoggedGuard]
  },
  { 
    path: 'lessons', 
    loadChildren: () => import('./protected/lessons/lessons.module').then(m => m.LessonsModule),
    canActivate: [IsLoggedGuard]
  },
  { path: '', redirectTo: '/lessons', pathMatch: 'full' },
  { path: '**', redirectTo: 'page-introuvable', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
