export const environment = {
  production: true,
  userUrl: 'http://localhost:8000/api/users/user',
  courseUrl: 'http://localhost:8000/api/course',
  lessonUrl: 'http://localhost:8000/api/lesson'
};
